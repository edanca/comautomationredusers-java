# AUTOMATION FRAMEWORK

This automtion framework is made by the Hybrid Driven Testing type.

## PLAN
### PAGE:
http://www.redusers.com/

## PHASE 1
### Objectives:
Bussiness Objectives:
- Registry as user
- Inform technology news (slide/acordion and outstanding)
- Redirect to social networks
- Registry as user at Usershop
- Registry as user of RedUsers Premium

### Functional Objectives:
- Test registry and user login
- Verify social networks links and icons
- Verify news (slide/acordion and outstanding) display
- Verify link and redirection to usershop
- Verify link and redirection to RedUsers Premium
- Informs Report out

### Milestones:
- Logging in file.
- Take screenshot of failures.
- Automate Functional objectives.
- Include Reports
- Implements Data Driven Testing

## Use cases by business objectives:
### Registry and login as user:
+ Navigate to Login page
    - validate page
+ Login
    - Happy path
    - Handle unexistent password
    - Handle unexistent user
    - Handle unexistent user and password
    - Use remember me on success login
    - Use remember me on unssucess login
    - Sanity check registration link
    - Sanity check lost password
    - Sanity check back to RedUsers
+ Social Networks
    - List and check social networks images
    - List and sanity check social networks links
+ Verify News
    - Compare slide elements with acordion element to verify has same quantity
    - Sanity check slider links
    - Sanity check acordeon's links
    - Validate outstanding news are visible.
    - Check outstanding links and sanity check.
+ Usershop
    - sanity check link
+ Redusers Premium
    - sanity check link

Technical Goals:
- Creation of test framework with its layers
- Implementation logging
- Implementation screen capture
- Implementation report out
- Implementation DAO (Data Access Objects)

Defining Layers:
- Test case layer
- Business layer
- Operations layer
- Pages/Entity/Resources
- helpers
- Data Access layer