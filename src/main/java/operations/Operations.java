package operations;

import org.openqa.selenium.WebDriver;
import pages.LoginPage;
import pages.HomePage;
import pages.RegisterPage;
import pages.RegisterSucsessPage;

import java.util.Map;

public class Operations {

    // ----------------------------------------------- //
    // VARIABLES SECTION
    // ----------------------------------------------- //

    private WebDriver driver;

    // ----------------------------------------------- //
    // PUBLIC METHODS SECTION
    // ----------------------------------------------- //

    public Operations(WebDriver driver) { this.driver = driver; }

    public void login_RedUsers(String user, String pass) {
        click_ingresar();
        // TO DO:
        //
    }

    public void login_UserShop() {

    }

    public void login_RedUsersPremium() {

    }

    public void register_RedUsers(Map userData) {
        boolean result;

        click_ingresar();
        result = verify_LoginPage();
        if (!result) { return; }

        click_registrarse();
        result = verify_RegisterPage("RedUSERS ‹ Formulario de registro");
        if (!result) { return; }

        RegisterPage registerPage = new RegisterPage(driver);
        registerPage.setUsername(userData.get("userName").toString());
        registerPage.setEmail(userData.get("email").toString());
        registerPage.setPass(userData.get("pass").toString(), userData.get("pass").toString());
        registerPage.setName(userData.get("name").toString());
        registerPage.setLastname(userData.get("lastName").toString());
        registerPage.setBirthdate(userData.get("birthDate").toString());
        registerPage.setGender(userData.get("gender").toString());
        registerPage.setCountry(userData.get("country").toString());
        registerPage.setLocation(userData.get("location").toString());
        registerPage.setState(userData.get("state").toString());
        registerPage.setNews(userData.get("news").toString());
        registerPage.setPromotions(userData.get("promotions").toString());

        registerPage.click_Registrarse();
    }


    // ----------------------------------------------- //
    // PRIVATE METHODS SECTION
    // ----------------------------------------------- //

    private void click_ingresar() {
        HomePage homePage = new HomePage(driver);
        homePage.click_btn_ingresar();
    }

    private void click_registrarse() {
        LoginPage loginPage = new LoginPage(driver);
        loginPage.clicklink_registrarse();
    }


    // ----------------------------------------------- //
    // VERIFICATIONS SECTION
    // ----------------------------------------------- //

    public boolean verify_LoginPage() {
        LoginPage page_login = new LoginPage(driver);
        boolean result = page_login.verify_pageTitle();
        return result;
    }

    public boolean verify_RegisterPage(String ex_pageTitle) {
        RegisterPage registerPage = new RegisterPage(driver);
        String title = registerPage.getPageTitle();

        if (title.equals(ex_pageTitle)) {
            return true;
        } else {
            return false;
        }
    }

    public boolean verify_RegisteredPage(String ex_pageTitle) {
        RegisterSucsessPage registerSucsessPage = new RegisterSucsessPage(driver);
        String title = registerSucsessPage.getPageTitle();

        if (title.equals(ex_pageTitle)) {
            return true;
        } else {
            return false;
        }
    }

    public boolean verify_userEmailAlreadyExists(String ex_errorText) {
        // INPUT: Expected Text Error
        // OUTPUT: TRUE or FALSE if the expected text is contained in the webellement's text

        // EXAMPLE expected text "Ese nombre de usuario ya está registrado. Por favor elige otro."
        // "Esa dirección de correo electrónico ya está registrada. Por favor, elige otra."

        RegisterPage registerPage = new RegisterPage(driver);
        String errorText = registerPage.getUserEmailAlreadyExistsErrorText();

        if (errorText.contains(ex_errorText)) {
            return true;
        } else {
            return false;
        }
    }

    public boolean verify_RegisterSuccessPage(String ex_pageTitle) {
        RegisterSucsessPage rp = new RegisterSucsessPage(driver);
        String title = rp.getPageTitle();

        if (title.equals(ex_pageTitle)) {
            return true;
        } else {
            return false;
        }
    }
}
