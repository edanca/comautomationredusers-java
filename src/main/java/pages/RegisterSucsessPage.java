package pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class RegisterSucsessPage extends BasePage {

    private WebDriver driver;

    public RegisterSucsessPage(WebDriver driver) {
        this.driver = driver;
    }


    // ----------------------------------------------- //
    // VERIFICATIONS SECTION
    // ----------------------------------------------- //
    public String getPageTitle() {
        return driver.getTitle();
    }
}
