package pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class HomePage extends BasePage {

    private WebDriver driver;
    private By btn_ingresar;

    public HomePage(WebDriver driver)
    {
        this.driver = driver;
        btn_ingresar = By.cssSelector("html body div#top_of_page.wrapper div.wrap_top header.header div.main_nav div.user_shop div.boton_login a");
    }

    public void click_btn_ingresar() {
        WebElement elem_btn_ingreser = driver.findElement(btn_ingresar);
        elem_btn_ingreser.click();
    }
}
