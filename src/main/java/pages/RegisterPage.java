package pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.pagefactory.AjaxElementLocatorFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;

public class RegisterPage extends BasePage {

    private WebDriver driver;

    @FindBy(name = "user_login")
    private WebElement elem_username;
    @FindBy(name = "user_email")
    private WebElement elem_email;
    @FindBy(id = "pass1")
    private WebElement elem_pass1;
    @FindBy(id = "pass2")
    private WebElement elem_pass2;
    @FindBy(id = "first_name")
    private WebElement elem_name;
    @FindBy(id = "last_name")
    private WebElement elem_lastName;
    @FindBy(id = "rpr_fecha_de_nacimiento_ddmmaa")
    private WebElement elem_birthDate;
    @FindBy(id = "rpr_sexo-m")
    private WebElement elem_masculino;
    @FindBy(id = "rpr_sexo-f")
    private WebElement elem_femenino;
    @FindBy(id = "rpr_pais")
    private WebElement elem_pais;
    @FindBy(name = "rpr_localidad")
    private WebElement elem_location;
    @FindBy(name = "rpr_provincia__estado")
    private WebElement elem_state;
    @FindBy(id = "rpr_newsletters-noticias")
    private WebElement elem_noticias;
    @FindBy (id = "rpr_newsletters-promociones")
    private WebElement elem_promociones;
    @FindBy (id = "wp-submit")
    private WebElement elem_btn_registrarse;
    @FindBy (id = "login_error")
    private WebElement elem_section_error_user_email_already_exists;

    public RegisterPage(WebDriver driver) {
        this.driver = driver;
        PageFactory.initElements(new AjaxElementLocatorFactory(driver, 15), this);
    }

    public void setUsername(String username) {
        elem_username.clear();
        elem_username.sendKeys(username);
    }

    public void setEmail(String email) {
        elem_email.clear();
        elem_email.sendKeys(email);
    }

    public void setPass(String pass1, String pass2) {
        elem_pass1.clear();
        elem_pass1.sendKeys(pass1);
        elem_pass2.clear();
        elem_pass2.sendKeys(pass2);
    }

    public void setName(String name) {
        elem_lastName.clear();
        elem_name.sendKeys(name);
    }

    public void setLastname(String lastname) {
        elem_lastName.clear();
        elem_lastName.sendKeys(lastname);
    }

    public void setBirthdate(String birthdate) {
        elem_birthDate.clear();
        elem_birthDate.sendKeys(birthdate);
    }

    public void setGender(String gender) {
        if (gender.equals("M")) {
            if (elem_masculino.isDisplayed()) {
                elem_masculino.click();
            }
        } else if (gender.equals("F")) {
            if (elem_femenino.isDisplayed()) {
                elem_femenino.click();
            }
        }
    }

    public void setCountry(String country) {
        Select select_pais = new Select(elem_pais);
        select_pais.selectByVisibleText(country);
    }

    public void setLocation(String location) {
        elem_location.clear();
        elem_location.sendKeys(location);
    }

    public void setState(String state) {
        elem_state.clear();
        elem_state.sendKeys(state);
    }

    public void setNews(String news) {
        if (elem_noticias.isDisplayed()) {
            if (news.equals("true") && !elem_noticias.isSelected()) {
                elem_noticias.click();
            } else if (news.equals("false") && elem_noticias.isSelected()) {
                elem_noticias.click();
            }
        }
    }

    public void setPromotions(String promotions) {
        if (elem_promociones.isDisplayed()) {
            if (promotions.equals("true") && !elem_promociones.isSelected()) {
                elem_promociones.click();
            } else if (promotions.equals("false") && elem_promociones.isSelected()) {
                elem_promociones.click();
            }
        }
    }

    public void click_Registrarse() {
        elem_btn_registrarse.click();
        super.sleep(3);
    }


    // ----------------------------------------------- //
    // VERIFICATIONS SECTION
    // ----------------------------------------------- //
    public String getPageTitle() {
        return driver.getTitle();
    }

    public String getUserEmailAlreadyExistsErrorText() {
        return elem_section_error_user_email_already_exists.getText();
    }
}
