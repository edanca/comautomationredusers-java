package pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.ArrayList;

public class BasePage {

    private WebDriver driver;
    private String url;
    private ArrayList<String> tabs;
    protected WebDriverWait wait;

    public void waiting() {
        this.wait = new WebDriverWait(driver, 10);
    }

    public void sleep(int seconds) {
        seconds = seconds * 1000;
        try {
            Thread.sleep(seconds);
        } catch (Exception e) {
            Thread.currentThread().interrupt();
        }
    }
}
