package pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import pages.BasePage;

public class LoginPage extends BasePage {

    private WebDriver driver;
    private By fieldtext_usuario;
    private By fieldtext_contrasena;
    private By btn_acceder;
    private By chkbx_recuerdame;
    private By section_loginError;

    @FindBy(css = "#nav > a:nth-child(1)")
    private WebElement elem_link_registrarse;

    public LoginPage(WebDriver driver) {
        this.driver = driver;
        PageFactory.initElements(driver, this);
        fieldtext_usuario = By.id("user_login");
        fieldtext_contrasena = By.id("user_pass");
        btn_acceder = By.id("wp-submit");
        chkbx_recuerdame = By.name("rememberme");
        section_loginError = By.id("login_error");
    }

    // ----------------------------------------------- //
    // ELEMENT INTERACTIONS
    // ----------------------------------------------- //
    public void enterText_usuario(String user) {
        WebElement elem_field_usuario = driver.findElement(fieldtext_usuario);
        elem_field_usuario.sendKeys(user);
    }

    public void enterText_contrasena(String pass) {
        WebElement elem_field_contrasena = driver.findElement(fieldtext_contrasena);
        elem_field_contrasena.sendKeys(pass);
    }

    public void clickbtn_acceder() {
        WebElement elem_btn_acceder = driver.findElement(btn_acceder);
        elem_btn_acceder.click();
    }

    public void selectcheckbox_recuerdame() {
        WebElement elem_chkbx_recuerdame = driver.findElement(chkbx_recuerdame);
        if (!elem_chkbx_recuerdame.isSelected()) {
            elem_chkbx_recuerdame.click();
        }
    }

    public void clicklink_registrarse() {
        elem_link_registrarse.click();
    }

    public boolean findElem_login_error() {
        try {
            WebElement elem_section_loginError = super.wait.until(ExpectedConditions.presenceOfElementLocated(section_loginError));
            return true;
        } catch (Exception e) {
            //e.printStackTrace();
            return false;
        }
    }


    // ----------------------------------------------- //
    // VERIFICATIONS SECTION
    // ----------------------------------------------- //
    public boolean verify_pageTitle() {
        String ex_pageTitle = (String) "RedUSERS ‹ Acceder";
        boolean result = false;

        if (driver.getTitle().equals(ex_pageTitle)) {
            result = true;
        } else {
            result = false;
        }
        return result;
    }

}
