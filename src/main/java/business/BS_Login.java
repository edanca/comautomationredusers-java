package business;

import operations.Operations;
import org.openqa.selenium.WebDriver;
import org.testng.Assert;

public class BS_Login {

    private WebDriver driver;
    private boolean result;

    public BS_Login(WebDriver driver) {
        this.driver = driver;
    }

    public void redUserslogin(String user, String pass) {
        Operations ops = new Operations(driver);
        ops.login_RedUsers(user, pass);

        result = ops.verify_LoginPage();

        Assert.assertTrue(result);
        if (result) {
            System.out.println("-------------Success-------------");
        }
    }
}
