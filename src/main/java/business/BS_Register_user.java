package business;

import operations.Operations;
import org.openqa.selenium.WebDriver;
import org.testng.Assert;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;

public class BS_Register_user {

    private WebDriver driver;
    private boolean result;

    public BS_Register_user(WebDriver driver) { this.driver = driver; }

    public void registerUser(HashMap userData) {
        Operations ops = new Operations(driver);
        ops.register_RedUsers(userData);
    }

    public void verify_userRegistered(String ex_text) {
        Operations ops = new Operations(driver);
        boolean result = ops.verify_RegisteredPage(ex_text);

        if(result) {
            Assert.assertTrue(result);
        } else {
            Assert.assertTrue(result);
        }
    }

    public void verifyError_userEmailAlreadyExist(String ex_textError) {
        Operations ops = new Operations(driver);

        boolean result = ops.verify_userEmailAlreadyExists(ex_textError);

        if (result == true) {
            Assert.assertTrue(result);
        } else {
            Assert.assertTrue(result);
        }
    }
}
