package testcases;

import business.BS_Login;
import business.BS_Register_user;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.TimeUnit;

public class TestCases {

    private WebDriver driver;

    private String url = "http://redusers.com";

    @BeforeClass
    public void setUpClass() {
        driver = new ChromeDriver();
        System.out.println("============ START TESTING ============");
    }

    @AfterClass
    public void tearDown() {
        driver.quit();
        System.out.println("============= END TESTING =============");
    }

    @BeforeMethod
    public void setUp() {
        driver.navigate().to(url);
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
    }

    // ==============================================================================
    // ====== TEST CASES ============================================================
    // ==============================================================================

    @Test(enabled = true, groups = {"regression", "login"})
    public void test_RedUsersLoginSuccess() {
        String user = "edd1234@yopmail.com";
        String pass = "123456";

        BS_Login bs_login = new BS_Login(driver);
        bs_login.redUserslogin(user, pass);
    }

    @Test(enabled = true, groups = {})
    public void test_RedUsersLoginFailure() {
        String user = "edanca@yopmail.com";
        String pass = "123456";

        BS_Login bs_login = new BS_Login(driver);
        bs_login.redUserslogin(user, pass);
    }

    @Test(enabled = false, groups = {"regression","register"})
    public void test_registration() {
        HashMap<String,String> userData = new HashMap<String, String>();
        userData.put("userName", "edd1234");
        userData.put("email", userData.get("userName") +"@yopmail.com");
        userData.put("pass", "123456");
        userData.put("name", userData.get("userName"));
        userData.put("lastName", "edd");
        userData.put("birthDate", "01/02/1990");
        userData.put("gender", "M");
        userData.put("country", "Argentina");
        userData.put("location", "CABA");
        userData.put("state", "CABA");
        userData.put("news", "true");
        userData.put("promotions", "true");

        BS_Register_user bs_register_user = new BS_Register_user(driver);
        bs_register_user.registerUser(userData);
    }

    @Test(enabled = true, groups = {})
    public void test_login_sucess() {
        String username = "edd123";
        String email = username + "@yopmail.com";
        String pass = "123456";
    }

    @Test(enabled = true, groups = {})
    public void test_register_userAlreadyRegistered() {
        String ex_errorText = "Ese nombre de usuario ya está registrado. Por favor elige otro.";

        HashMap<String,String> userData = new HashMap<String, String>();
        userData.put("userName", "edd1234");
        userData.put("email", userData.get("userName").toString() + "123" +"@yopmail.com");
        userData.put("pass", "123456");
        userData.put("name", userData.get("userName"));
        userData.put("lastName", "edd");
        userData.put("birthDate", "01/02/1990");
        userData.put("gender", "M");
        userData.put("country", "Argentina");
        userData.put("location", "CABA");
        userData.put("state", "CABA");
        userData.put("news", "true");
        userData.put("promotions", "true");

        BS_Register_user bs_register_user = new BS_Register_user(driver);
        bs_register_user.registerUser(userData);
        bs_register_user.verifyError_userEmailAlreadyExist(ex_errorText);
    }

    @Test(enabled = true, groups = {})
    public void test_register_emailAlreadyRegistered() {
        String ex_errorText = "Esa dirección de correo electrónico ya está registrada. Por favor, elige otra";

        HashMap<String,String> userData = new HashMap<String, String>();
        userData.put("userName", "edd1234");
        userData.put("email", "edd1234@yopmail.com");
        userData.put("pass", "123456");
        userData.put("name", userData.get("userName"));
        userData.put("lastName", "edd");
        userData.put("birthDate", "01/02/1990");
        userData.put("gender", "M");
        userData.put("country", "Argentina");
        userData.put("location", "CABA");
        userData.put("state", "CABA");
        userData.put("news", "true");
        userData.put("promotions", "true");

        BS_Register_user bs_register_user = new BS_Register_user(driver);
        bs_register_user.registerUser(userData);
        bs_register_user.verifyError_userEmailAlreadyExist(ex_errorText);
    }
}
